package com.predapp.multitenant.profile.config;

import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = "com.predapp.multitenant.profile")
public class FeignConfiguration {

}
