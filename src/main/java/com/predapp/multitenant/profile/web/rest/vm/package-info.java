/**
 * View Models used by Spring MVC REST controllers.
 */
package com.predapp.multitenant.profile.web.rest.vm;
